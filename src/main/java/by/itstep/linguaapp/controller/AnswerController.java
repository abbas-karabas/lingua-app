package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.answers.AnswerFullDto;
import by.itstep.linguaapp.dto.answers.AnswerUpdateDto;
import by.itstep.linguaapp.service.AnswerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class AnswerController {

    private final AnswerService answerService;

    @PutMapping("/answers")
    public AnswerFullDto update(@Valid @RequestBody AnswerUpdateDto dto) {
        return answerService.update(dto);
    }

    @DeleteMapping("/answers/{id}")
    public void delete(Integer id) {
        answerService.delete(id);
    }
}
