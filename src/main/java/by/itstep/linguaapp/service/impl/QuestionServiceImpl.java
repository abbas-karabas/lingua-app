package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.answers.AnswerCreateDto;
import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;
import by.itstep.linguaapp.entity.AnswerEntity;
import by.itstep.linguaapp.entity.CategoryEntity;
import by.itstep.linguaapp.entity.QuestionEntity;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.mapper.QuestionMapper;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.repository.QuestionRepository;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.service.MailService;
import by.itstep.linguaapp.security.AuthenticationService;
import by.itstep.linguaapp.service.QuestionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ValidationException;
import java.time.Instant;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService {


    private final QuestionRepository questionRepository;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final AnswerRepository answerRepository;
    private final QuestionMapper questionMapper;
    private final MailService mailService;
    private final AuthenticationService authenticationService;

    @Override
    @Transactional
    public QuestionFullDto create(QuestionCreateDto dto) {
        throwIfInvalidNumberOfCorrectAnswers(dto);

        QuestionEntity questionToSave = questionMapper.map(dto);
        for (AnswerEntity answer : questionToSave.getAnswers()) {
            answer.setQuestion(questionToSave);
        }

        List<CategoryEntity> categoriesToAdd = categoryRepository.findAllById(dto.getCategoriesIds());
        questionToSave.setCategories(categoriesToAdd);

        QuestionEntity savedQuestion = questionRepository.save(questionToSave);

        QuestionFullDto questionDto = questionMapper.map(savedQuestion);
        return questionDto;
    }

    @Override
    @Transactional
    public QuestionFullDto update(QuestionUpdateDto dto) {
        QuestionEntity questionToUpdate = questionRepository.findById(dto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException("QuestionEntity was not found by id " + dto.getId()));

        questionToUpdate.setDescription(dto.getDescription());
        questionToUpdate.setLevel(dto.getLevel());

        QuestionEntity updateQuestion = questionRepository.save(questionToUpdate);
        QuestionFullDto questionDto = questionMapper.map(updateQuestion);

        log.info("Question was successfully updated");
        return questionDto;
    }

    @Override
    @Transactional
    public QuestionFullDto findById(Integer id) {
        QuestionEntity foundQuestion = questionRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("QuestionEntity was not found by id " + id));

        QuestionFullDto questionDto = questionMapper.map(foundQuestion);
        log.info("Question was successfully found");
        return questionDto;
    }

    @Override
    @Transactional(readOnly = true)
    public List<QuestionShortDto> findAll() {
        List<QuestionEntity> foundQuestions = questionRepository.findAll();
        List<QuestionShortDto> dtos = questionMapper.map(foundQuestions);
        log.info(dtos.size() + " Questions were found");
        return dtos;
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        QuestionEntity questionToDelete = questionRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("QuestionsEntity was not found by id " + id));

        questionToDelete.setDeletedAt(Instant.now());
        for (AnswerEntity answer : questionToDelete.getAnswers()) {
            answer.setDeletedAt(Instant.now());
        }
        questionRepository.save(questionToDelete);

        log.info("Question was successfully deleted");
    }

    @Override
    @Transactional
    public boolean checkAnswer(Integer questionId, Integer answerId) {
        AnswerEntity answer = answerRepository.findByIdAndQuestionId(answerId, questionId);
        if (answer == null) {
            throw new AppEntityNotFoundException("Answer was not found by id: " + answerId + " in the question: " + questionId);
        }
        UserEntity user = authenticationService.getAuthenticatedUser();
        user.setLastAnswerDate(Instant.now());
        userRepository.save(user);

        if (answer.getCorrect()) {
            QuestionEntity question = answer.getQuestion();
            question.getUsersWhoCompleted().add(user);
            questionRepository.save(question);
            mailService.sendEmail(user.getEmail(), "Good Job! Right answer!");


        }
        return answer.getCorrect();
    }

    @Override
    @Transactional
    public QuestionFullDto getRandomQuestion(Integer categoryId) {
        UserEntity user = authenticationService.getAuthenticatedUser();
        List<QuestionEntity> foundQuestions = questionRepository.findNotCompleted(categoryId, user.getId());

        if (foundQuestions.isEmpty()) {
            throw new AppEntityNotFoundException("Can't find available questions by category id: " + categoryId);
        }
        int randomIndex = (int) (foundQuestions.size() * Math.random());
        QuestionEntity randomQuestion = foundQuestions.get(randomIndex);
        log.info("Random question was successfully found");
        return questionMapper.map(randomQuestion);
    }

    private void throwIfInvalidNumberOfCorrectAnswers(QuestionCreateDto dto) {
        int counter = 0;
        for (AnswerCreateDto answer : dto.getAnswers()) {
            if (answer.getCorrect()) {
                counter++;
            }
        }
        if (counter != 1) {
            throw new ValidationException("Question must contains the only one correct answer");
        }
    }
}
