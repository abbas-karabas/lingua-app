package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.exception.UniqueValuesIsTakenExceptions;
import by.itstep.linguaapp.exception.WrongUserPasswordException;
import by.itstep.linguaapp.mapper.UserMapper;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public UserFullDto create(UserCreateDto createDto) {
        UserEntity entityToSave = userMapper.map(createDto);
        entityToSave.setBlocked(false);
        entityToSave.setRole(UserRole.USER);
        entityToSave.setPassword(passwordEncoder.encode(createDto.getPassword() + createDto.getEmail()));

        Optional<UserEntity> entityWithSameEmail = userRepository.findByEmail(entityToSave.getEmail());
        if (entityWithSameEmail.isPresent()) {
            throw new UniqueValuesIsTakenExceptions("Email is taken!");
        }
        UserEntity savedEntity = userRepository.save(entityToSave);
        UserFullDto userDto = userMapper.map(savedEntity);

        log.info("User was successfully created");
        return userDto;
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto dto) {
        UserEntity userToUpdate = userRepository.findById(dto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id " + dto.getId()));

        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        userToUpdate.setCountry(dto.getCountry());
        userToUpdate.setName(dto.getName());
        userToUpdate.setPhone(dto.getPhone());

        UserEntity updateUser = userRepository.save(userToUpdate);
        UserFullDto userDto = userMapper.map(updateUser);

        log.info("User was successfully updated");
        return userDto;
    }

    @Override
    @Transactional
    public UserFullDto findById(int id) {
        UserEntity foundUser = userRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id " + id));

        UserFullDto userDto = userMapper.map(foundUser);
        log.info("User was successfully found");
        return userDto;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserFullDto> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);

        Page<UserEntity> foundUsers = userRepository.findAll(pageable);

        Page<UserFullDto> foundDtos = foundUsers.map(userEntity -> userMapper.map(userEntity));
        log.info(foundDtos.getNumberOfElements() + " Users were found");
        return foundDtos;
    }

    @Override
    @Transactional
    public void delete(int id) {
        UserEntity userToDelete = userRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id " + id));

        userToDelete.setDeletedAt(Instant.now());
        userRepository.save(userToDelete);
        log.info("User was successfully deleted");
    }

    @Override
    @Transactional
    public void changePassword(ChangeUserPasswordDto dto) {
        UserEntity userToUpdate = userRepository.findById(dto.getUserId())
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id " + dto.getUserId()));

        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        if (!passwordEncoder.matches((dto.getOldPassword() + userToUpdate.getEmail()), userToUpdate.getPassword()))  {
            throw new WrongUserPasswordException("Wrong password");
        }
        userToUpdate.setPassword(passwordEncoder.encode(dto.getNewPassword() + userToUpdate.getEmail()));
        userRepository.save(userToUpdate);

        log.info("User password was successfully updated");
    }

    @Override
    @Transactional
    public UserFullDto changeRole(ChangeUserRoleDto dto) {
        UserEntity userToUpdate = userRepository.findById(dto.getUserId())
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id " + dto.getUserId()));

        userToUpdate.setRole(dto.getNewRole());

        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDto userDto = userMapper.map(updatedUser);
        log.info("User role was successfully updated");
        return userDto;
    }

    @Override
    @Transactional
    public void block(Integer userId) {
        UserEntity entityToUpdate = userRepository.findById(userId)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id " + userId));

        entityToUpdate.setBlocked(true);
        userRepository.save(entityToUpdate);
        log.info("User was successfully updated.");
    }

    private boolean currentUserIsAdmin() {
        return SecurityContextHolder.getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .anyMatch(ga ->  ga.getAuthority().equals("ROLE_" + UserRole.ADMIN.name()));
    }

    private void throwIfNotCurrentUserOrAdmin(String email) {
        if (!currentUserIsAdmin()) {
            String currentUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            if (!currentUserEmail.equals(email)) {
                throw new RuntimeException("Can't update! Has no permission!");
            }
        }
    }
}

